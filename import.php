<?php

include 'config.php';
include 'db.php';


# импорт из 1-го файла
try {
    $fp = fopen("./stat1.txt", "r");
    while (!feof($fp)) {
        try {
            $str = fgets($fp);
            $_ = explode('|', trim($str));
            $data = [
                'datetime' => $_[0] . ' ' . $_[1],
                'ip' => $_[2],
                'url_source' => $_[3],
                'url_target' => $_[4],
            ];
            DB::insert('stat_url', $data);
        } catch (Exception $e) {
            echo 'Ошибка добавления записи: ' . $e->getMessage();
        }
    }
    fclose($fp);
} catch (Exception $e) {
    echo 'Ошибка чтения файла: ' . $e->getMessage();
}


# импорт из 2-го файла
try {
    $fp = fopen("./stat2.txt", "r");
    while (!feof($fp)) {
        try {
            $str = fgets($fp);
            $_ = explode('|', trim($str));
            $data = [
                'ip' => $_[0],
                'browser' => $_[1],
                'os' => $_[2],
            ];
            DB::insert('stat_browser', $data);
        } catch (Exception $e) {
            echo 'Ошибка добавления записи: ' . $e->getMessage();
        }
    }
    fclose($fp);
} catch (Exception $e) {
    echo 'Ошибка чтения файла: ' . $e->getMessage();
}
