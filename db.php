<?php

class DB
{
    private static $_conn;

    private function __construct()
    {
    }


    /**
     * Подключение к БД
     *
     * @return PDO
     */
    public static function conn()
    {
        $config = $GLOBALS['config'];

        if (!isset(self::$_conn)) {
            self::$_conn = new PDO($config['connectionString'], $config['username'], $config['password']);
            self::$_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$_conn;
    }


    /**
     * Управление транзациями
     */
    public static function beginTransaction()
    {
        self::conn()->beginTransaction();
    }

    public static function commit()
    {
        self::conn()->commit();
    }

    public static function rollBack()
    {
        self::conn()->rollBack();
    }


    /**
     * Создание записи
     *
     * @param $table string - название таблицы
     * @param $data array - ассоциативный массив данных
     * @return int - ID созданной записи
     */
    public static function insert($table, $data)
    {
        $fields = array_keys($data);
        $sql = "insert into " . $table . " (" . implode(', ', $fields) . ") values (:" . implode(', :', $fields) . ")";
        return self::query($sql, $data);
    }


    /**
     * Создание нескольких записей
     *
     * @param $table string - название таблицы
     * @param $data array - ассоциативный массив данных
     * @param $ignore - игнорировать вставку забисей с дубликатоми уникальных ключей
     */
    public static function multi_insert($table, $data, $ignore = false)
    {
        $keys = array_keys($data[0]);
        $values = array();
        $params = array();
        foreach ($data as $n => $_) {
            $value = array();
            foreach ($keys as $key) {
                $value[] = ':' . $key . $n;
                $params[$key . $n] = array_key_exists($key, $_) ? $_[$key] : null;
            }
            $values[] = '(' . implode(', ', $value) . ')';
        }
        $sql = "insert " . ($ignore ? "ignore" : "") . " into " . $table . " (" . implode(', ', $keys) . ") values " . implode(', ', $values);
        self::query($sql, $params);
    }


    /**
     * Обновление записи
     *
     * @param $table string - название таблицы
     * @param $data array - ассоциативный массив данных
     * @param $id int - идентификатор записи
     * @param bool $logging - логировать операцию
     * @return bool - результат выполнения операции
     */
    public static function update($table, $data, $id, $logging = true)
    {
        unset($data['id']);

        $set = array();
        foreach ($data as $fieldName => $fieldData) {
            $set[] = $fieldName . ' = :' . $fieldName;
            $params[$fieldName] = $fieldData;
        }
        $sql = "update " . $table . " set " . implode(', ', $set) . " where id = :id";
        $params['id'] = $id;

        return self::query($sql, $params);
    }


    /**
     * Удаление записи
     *
     * @param $table
     * @param $id
     * @return int
     */
    public static function delete($table, $id)
    {
        return self::query("delete from $table where id = :id", ['id' => $id]);
    }


    /**
     * Получение списка записей
     *
     * @param $sql - запрос
     * @param $params - параметры запроса
     * @return array - результат запроса
     */
    public static function select($sql, $params = array())
    {
        $sth = self::conn()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * Выполнение произвольного запроса
     *
     * @param $sql - запрос
     * @param $params - параметры запроса
     * @return int - кол-во измененных строк
     */
    public static function query($sql, $params = array())
    {
        $sth = self::conn()->prepare($sql);
        $sth->execute($params);
        return $sth->rowCount();
    }


}