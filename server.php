<?php

include 'config.php';
include 'db.php';

try {
    $conditions = $params = [];
    if (array_key_exists('query', $_GET)) {
        $conditions[] = 'stat_browser.ip = :ip';
        $params['ip'] = $_GET['query'];
    }

    $order = 1;
    if (array_key_exists('sort', $_GET)) {
        $sort = json_decode($_GET['sort'], true);
        switch ($sort[0]['property']) {
            case 'browser':
                $order = 'stat_browser.browser ' . ($sort[0]['direction'] == 'DESC' ? 'desc' : 'asc');
                break;
            case 'os':
                $order = 'stat_browser.os ' . ($sort[0]['direction'] == 'DESC' ? 'desc' : 'asc');
                break;
        }
    }

    $limit = (int)$_GET['limit'] ?? 25;
    $offset = (int)$_GET['start'] ?? 0;

    $sql = 'select stat_browser.*,
            (select stat_url.url_source from stat_url where stat_url.ip = stat_browser.ip order by stat_url.datetime limit 1) as first_source_url,
            (select stat_url.url_target from stat_url where stat_url.ip = stat_browser.ip order by stat_url.datetime desc limit 1) as last_target_url,
	        (select count(distinct stat_url.url_target) from stat_url where stat_url.ip = stat_browser.ip) as count_unique_url
        from stat_browser
        where ' . (!empty($conditions) ? implode(' and ', $conditions) : '1 = 1') . '
        order by ' . $order . '
        limit ' . $limit . '
        offset ' . $offset;
    $items = DB::select($sql, $params);
    echo json_encode([
        'success' => true,
        'items' => $items,
    ]);
} catch (Exception $e) {
    echo json_encode([
        'success' => false,
        'error' => $e->getMessage(),
    ]);
}