CREATE TABLE stat_browser
(
  ip character varying,
  browser character varying,
  os character varying
);


CREATE TABLE stat_url
(
  datetime timestamp without time zone,
  ip character varying,
  url_source character varying,
  url_target character varying
);
