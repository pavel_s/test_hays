Ext.Loader.setPath('Ext.ux', '/extjs/ux');
Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    name: 'app',
    requires: [
        'Ext.ux.form.SearchField',
        'app.data.model',
        'app.data.store',
        'app.view.grid',
        'app.view.win'
    ],
    appFolder: 'js',
    launch: function () {
        var win = Ext.create('app.view.win');
        var grid = win.down('grid');
        grid.store.loadPage(1);
        win.show();
    }
});
