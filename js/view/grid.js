Ext.define('app.view.grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid',
    initComponent: function () {
        this.store = Ext.create('app.data.store');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        labelWidth: 70,
                        fieldLabel: 'Фильтр по IP',
                        xtype: 'searchfield',
                        width: 300,
                        store: this.store,
                        padding: 5
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom'
            }
        ];
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'IP-адрес',
            dataIndex: 'ip',
            sortable: false,
            width: 70
        },
        {
            text: 'браузер',
            dataIndex: 'browser',
            flex: 1
        },
        {
            text: 'ос',
            dataIndex: 'os',
            flex: 1
        },
        {
            text: 'URL с которого зашел первый раз',
            dataIndex: 'first_source_url',
            sortable: false,
            flex: 2
        },
        {
            text: 'URL на который зашел последний раз',
            dataIndex: 'last_target_url',
            sortable: false,
            flex: 2
        },
        {
            text: 'кол-во просмотренных уникальных URL-адресов',
            dataIndex: 'count_unique_url',
            sortable: false,
            flex: 1
        }
    ]
});

