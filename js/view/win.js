Ext.define('app.view.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.win',
    title: 'Статистика запросов',
    layout: 'fit',
    height: 400,
    width: 800,
    maximizable: true,
    minimizable: true,
    items: [
        {
            xtype: 'grid',
            border: 0,
            padding: 0
        }
    ],
    listeners: {
        minimize: function () {
            var win = this;
            win.toggleCollapse();
        }
    }
});