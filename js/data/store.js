Ext.define('app.data.store', {
    extend: 'Ext.data.Store',
    model: 'app.data.model',
    remoteSort: true,
    proxy: {
        type: 'rest',
        url: '/server.php',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});