Ext.define('app.data.model', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ip', type: 'string'},
        {name: 'browser', type: 'string'},
        {name: 'os', type: 'string'},
        {name: 'first_source_url', type: 'string'},
        {name: 'last_target_url', type: 'string'},
        {name: 'count_unique_url', type: 'int'}
    ]
});
